from django.http import HttpResponse
import json


def get(function):
    def wrap(request, *args, **kwargs):
        if request.method == 'GET':
            return function(request, *args, **kwargs)
        else:
            return HttpResponse(status=405) # Retorna Método Invalido
    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap

def delete(function):
    def wrap(request, *args, **kwargs):
        if request.method == 'DELETE':
            return function(request, *args, **kwargs)
        else:
            return HttpResponse(status=405) # Retorna Método Invalido
    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap


def post(function):
    def wrap(request, *args, **kwargs):
        data=json.loads(request.body.decode('utf-8'))
        if request.method == 'POST' and bool(request.body):
            return function(request,data=data, *args, **kwargs)
        elif not bool(request.body):
            return HttpResponse(status=400) # Request Mal Formado
        else:
            return HttpResponse(status=405) # Retorna Método Invalido
    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap