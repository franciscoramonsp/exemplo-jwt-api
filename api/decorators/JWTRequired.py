from django.http import JsonResponse
from api.helpers import JWT,RetornoPadrao

def _dump_token(request):
    auth_header = request.META.get('HTTP_AUTHORIZATION',None)
    if not (auth_header and request.META['HTTP_AUTHORIZATION'].split(" ")[0] == "Bearer"):
        return {'status':False}
    user_token = request.META['HTTP_AUTHORIZATION'].split(" ")[1] # remove Bearer 
    jwt = JWT()
    return jwt.checa_token(user_token)

def _porteiro( cargo, permitidos = [] ,proibidos = [] ):
    if permitidos:
        return bool([ p for p in permitidos if p == cargo ])
    elif proibidos:
        return not bool([ p for p in proibidos if p == cargo ])
    else:
        return True

def jwt_required(permitidos = [],proibidos = []):
    def _wraps_method(function):
        def _wrap_args(request, *args, **kwargs):

            jwt_dump = _dump_token(request)
            if jwt_dump['status'] and _porteiro(
                cargo=jwt_dump['cargo'],
                permitidos=permitidos,
                proibidos=proibidos):
                return function(request, usuario=jwt_dump['email'] ,*args, **kwargs)
            else:
                return JsonResponse(RetornoPadrao.auth_invalida(),status=403) # Retorna Forbidden
        return _wrap_args
    return _wraps_method