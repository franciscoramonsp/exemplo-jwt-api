from enum import Enum


class CargosEmpresaEnum(Enum):
    ADMIN   = "ADMIN"
    GERENTE = "GERENTE"
    CLIENTE = "CLIENTE"