class RetornoPadrao:
    """
    Padroniza os Retornos
    """
    @staticmethod
    def retorno_padrao(msg:str, status: bool, error=None) -> dict:
        if status:
            return {'msg':msg,'status':status}
        else:
            return {'msg':'error','status':status,'error':msg}

    @staticmethod
    def ok(adicional:dict={}) -> dict :
        return dict(RetornoPadrao.retorno_padrao('ok',True),**adicional)

    @staticmethod
    def fail(causa="") -> dict :
        return dict(RetornoPadrao.retorno_padrao(causa,False))

    @staticmethod
    def token_invalida() -> dict:
        return RetornoPadrao.fail('Token Invalida')

    @staticmethod
    def token_expirada() -> dict:
        return RetornoPadrao.fail('Token Expirada')
    
    @staticmethod
    def auth_invalida() -> dict:
        return RetornoPadrao.fail('Autenticação Invalida')

    @staticmethod
    def token_ok() -> dict :
        return RetornoPadrao.ok()

    @staticmethod
    def auth_ok(adicional:dict={}) -> dict:
        return dict(RetornoPadrao.ok(),**adicional)