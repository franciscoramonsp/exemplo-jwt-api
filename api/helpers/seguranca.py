from jwt.exceptions import ExpiredSignatureError,DecodeError
from api.helpers import RetornoPadrao
from datetime import datetime, timedelta
import hashlib, jwt


class Password:

    @staticmethod
    def encrypt( password:str ) -> str:
        """
        Receber um String e retorna criptografada em sha1
        """
        return hashlib.sha1(password.encode('utf-8')).hexdigest()

class JWT:
    """
    Gerencia o sistema de JWT
    """

    _secret = "5Hfld3oA9GQEubb!6!5$"
    _alg    = "HS256"

    _payload = {
        'iss': "urn:commerce-api",  # Emissor
        'exp': None,                # Tempo de Expiração
        'iat': None,                # Tempo de Criaço
        'email': None,
        'cargo': None,
    }

    def gera_token(self,usuario=None,timeout_min=60) -> str:
        self._payload['iat'] = datetime.utcnow()
        self._payload['exp'] = datetime.utcnow() + timedelta(minutes=timeout_min)
        self._payload['email'] = usuario.email
        self._payload['cargo'] = usuario.cargo

        return jwt.encode(self._payload, self._secret,algorithm=self._alg).decode('utf-8')

    def checa_token(self, token) -> dict:
        try:
            return dict(
                jwt.decode(token, self._secret, algorithms=[self._alg], issuer=self._payload['iss']),
                **RetornoPadrao.token_ok()
            )
        except ExpiredSignatureError:
            return RetornoPadrao.token_expirada()
        except DecodeError:
            return RetornoPadrao.token_invalida()