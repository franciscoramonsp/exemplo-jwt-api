from django.db import models
from api.helpers import CargosEmpresaEnum


class Empresa(models.Model):
    nome = models.CharField(max_length=50)    
    cnpj = models.CharField(max_length=20)
    cidade = models.CharField(max_length=50)
    estado = models.CharField(max_length=2)
    
class Usuario(models.Model):
    nome = models.CharField(max_length=50)    
    email = models.EmailField(max_length=254)
    password = models.CharField(max_length=40)
    empresa = models.ForeignKey(Empresa, on_delete=models.CASCADE)
    cargo = models.CharField(max_length=50,choices=[(cargo,cargo.value) for cargo in CargosEmpresaEnum])

class Produto(models.Model):
    nome = models.CharField(max_length=50)
    descricao = models.TextField()
    preco = models.DecimalField(max_digits=8, decimal_places=2)
    quantidade = models.PositiveIntegerField()