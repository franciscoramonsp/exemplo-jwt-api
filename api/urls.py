from django.urls import path
from .views import *

urlpatterns = [
    
    # Auth
    path('auth/login', auth),
    path('auth/user', usuario_logado),
    path('auth/users', usuarios),

    # Admin
    path('admin/user/new',add_usuario),


    # Produtos
    path('produtos',list_produtos),
    path('produto/<int:id_produto>',show_produto),
    path('produto/new',add_produto),
    path('produto/update/<int:id_produto>',add_produto),
    path('produto/delete/<int:id_produto>',delete_produtos)

]