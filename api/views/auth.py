from django.http import JsonResponse
from django.core.exceptions import ObjectDoesNotExist
from api.helpers import JWT,RetornoPadrao,Password,Conversor
from api.decorators import jwt_required,post,get
from api.models import Usuario

@post
def auth(request,data):
    try:
        usuario = Usuario.objects.get(email=data['email'],password=Password.encrypt(data['senha']))
        jwt_token = JWT().gera_token(usuario=usuario)
        return JsonResponse(RetornoPadrao.auth_ok({'token':jwt_token,'cargo':usuario.cargo}), status = 202)
    except ObjectDoesNotExist:
        return JsonResponse(RetornoPadrao.auth_invalida(),status=403)

@get
@jwt_required()
def usuario_logado(request,usuario): # usuario_logado injetado pelo jwt_required
    usuario_logado = Usuario.objects.get(email=usuario)
    usuario = Conversor.model_dict(usuario_logado,campos=['id','nome','email','cargo'])
    return JsonResponse(RetornoPadrao.auth_ok(usuario),status=200,safe=False)


@get
@jwt_required(permitidos=['ADMIN'])
def usuarios(request,usuario): # usuario_logado injetado pelo jwt_required
    usuarios_filter = Usuario.objects.all()
    usuarios = Conversor.models_dict(usuarios_filter,campos=['id','nome','email','cargo'])
    return JsonResponse(RetornoPadrao.auth_ok({'usuarios':usuarios}),status=200,safe=False)

@post
@jwt_required(permitidos=['ADMIN'])
def add_usuario(request,data,usuario):
    print(data)
    usuario = Usuario(**data)
    usuario.password =Password.encrypt(data['password'])
    usuario.empresa_id = 1
    usuario.save()
    return JsonResponse(RetornoPadrao.auth_ok(),status=200,safe=False)
