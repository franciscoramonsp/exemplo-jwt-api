from django.http import JsonResponse
from django.core.exceptions import ObjectDoesNotExist
from api.helpers import JWT,RetornoPadrao,Password,Conversor
from api.decorators import jwt_required,post,get,delete
from api.models import Produto

@get
def list_produtos(request):
    produtos_filter = Produto.objects.all()
    produtos = Conversor.models_dict(produtos_filter)
    return JsonResponse(RetornoPadrao.ok({'produtos':produtos}),status=202,safe=False)

@get
def show_produto(request,id_produto):
    try:
        produto_filter = Produto.objects.get(pk=id_produto)
        produtos = Conversor.model_dict(produto_filter)
        return JsonResponse(RetornoPadrao.ok({'produto':produtos}),status=202,safe=False)
    except ObjectDoesNotExist:
        return JsonResponse(RetornoPadrao.fail("Produto Não Existente"),status=404)

@post
@jwt_required(permitidos=['ADMIN','GERENTE'])
def add_produto(request,data,usuario):
    produto = Produto(**data)
    produto.save()
    return JsonResponse(RetornoPadrao.ok(),status=202,safe=False)

@post
@jwt_required(permitidos=['ADMIN','GERENTE'])
def update_produto(request,data,usuario,id_produto):
    pass

@delete
@jwt_required(permitidos=['ADMIN'])
def delete_produtos(request,usuario,id_produto):
    pass